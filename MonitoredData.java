import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {
	public String startTime;
	public String endTime;
	public String activity;
	public MonitoredData(String startTime,String endTime,String activity) {
		this.startTime=startTime;
		this.endTime=endTime;
		this.activity=activity;
	}
	public String getStart() {
		return startTime;
	}
	public String getStop() {
		return endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setStart(String Start) {
		this.startTime=Start;
	}
	public void setStop(String Stop) {
		this.endTime=Stop;
	}
	public void setActivity(String activity) {
		this.activity=activity;
	}
	public String toString() {
		return "start time: "+startTime+" end time: "+endTime+" activity: "+activity;
	}
	public int getZi() {
		String[] split1 = getStart().split(" ");
		String[] split2 = split1[0].split("-");
		int day = Integer.parseInt(split2[2]);
		return day;
	}
	public Long getDuration(){
		DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime date1 = LocalDateTime.from(f.parse(startTime));
		LocalDateTime date2 = LocalDateTime.from(f.parse(endTime));
		Duration d = Duration.between(date1, date2);
		
		return d.getSeconds()/60;
		
		
	}
}
