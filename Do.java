import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Do {
	private static final String file = "Activities.txt";
	private static final String FILENAME = "out.txt";

	ArrayList<MonitoredData> monitoredData;
	BufferedWriter bw = null;
	FileWriter fw = null;
	public Do() {
		this.monitoredData=new ArrayList<MonitoredData>();
		try {
			fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void read() {
		ArrayList<MonitoredData> data=new ArrayList<MonitoredData>();
		List<String> buf = null;
		try (Stream<String> stream=Files.lines(Paths.get(file))){
			buf = stream.collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (String i : buf){
			String[] split1 = i.split(" ");
			String[] split2 = split1[1].split("		");
			String[] split3 = split1[2].split("		");
			data.add(new MonitoredData(split1[0]+" " + split2[0],split2[1]+ " " + split3[0],split3[1]));
		}
		monitoredData=data;
	}
	public void countDistinctDays() {
		int days=0;
		ArrayList<String> list = new ArrayList<String>();
		for (MonitoredData m : monitoredData) {
			String[] splits = m.getStart().split(" ");
			list.add(splits[0]);
		}
		for (int i = 1; i < list.size(); i++) {
			int j=2;
			if (list.get(i).equals(list.get(j)) == true) {
				days++;
			
			}
			j++;
		}
		System.out.println("1. Number of distinct days: " + days);
		try {
			bw.write("1. Number of distinct days: " + days);
			bw.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void countDifActiv() {
		try {
			bw.write("\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bw.write("2. Number of occurences of each action:");
			bw.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("2. Number of occurences of each action:"); 
		Map<String, Long> map1 =
				monitoredData.parallelStream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		map1.entrySet().stream().forEach(e -> System.out.println(e.getValue() + "\t" + e.getKey()));
		Map<String, Long> map =
				monitoredData.parallelStream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		map.entrySet().stream().forEach(e -> write(e.getValue() + "\t" + e.getKey()));
	}
	public void Activity() {
		write("\n");
		write("3. Activity count for each day of the log:");
		System.out.println("3. Activity count for each day of the log:");
		Map<Integer, Map<String, Long>> map = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getZi,
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));

		map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEachOrdered(e -> write(e.getKey() + "\t" + e.getValue()));
		map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEachOrdered(e -> System.out.println(e.getKey() + "\t" + e.getValue()));
		try {
			bw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
	}
	public void write(String m) {
		try {
			bw.write(m);
			bw.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
